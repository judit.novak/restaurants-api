# The Restaurants API
 
## Description

A small test project implementing a web API written usign **Django REST Framework**. 

The idea is to manage a list of restaurants (practically: strings).

The web service only consists of one endpoint and the generated documentation.


## Usage

If installed as a package, the API could be run locally using

```
$ restaurants-api-local-run runserver
```

Note that you are likely to have to run 

```
$ restaurants-api-local-run migrate
```
first, to apply the database setup.

### Interactive documentation

This will start up a local webserver running the app on http://localhost:8000 by default.

Full description of API is now available at '/docs' following the base URL. **Django REST Framework** generated docs provide detailed description of the API endpoints, and even more!
Each functionality can be invoked interacively from the documentation page. This allows for an easy and intuitive and complete introduction to the API.


### The (only) endpoint

The API is only implementing one endpoint (`/restaurants`) which is to represent the simple restaurant objects. Restaurants only have a name and an `id`, not very sophisticated structures...
Basic listing and individual access, modification, create and delete functionalities are all available (even interactively, if the accessing the endpoint using a web browser).

Furthermore there is a `/restaurants/random` functionality, that returns a random restaurant from all those that are available.

A handy filter option is provided, that allows to search for a restaurant by name.

It's possible to search for any strings (case insenstive) across the whole dataset. 


## Important Notes

The project is **NOT** suitable for production in the current form. The `DEBUG` option has to be disabled in `restaurants/settings.py`.

In case automatic documentation is still to be used, static files have to be made available (see http://docs.djangoproject.com/en/2.1/howto/static-files/deployment/)


## External dependencies

 - `sqlite`


