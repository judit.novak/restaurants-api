"""Django Model representation of Restaurant data structures"""
from django.db import models

class Restaurant(models.Model):
    """Django model for the Restaurant object data store"""
    restaurant = models.CharField(max_length=100, blank=False, null=False)
